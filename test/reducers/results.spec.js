import expect from 'expect';
import results from '../../reducers/results';
import * as types from '../../constants/ActionTypes';
import {testInitState, testInitStateResult} from './results.initState';
import lodash from 'lodash';

describe('results reducer', () => {
  it('reducer should handle initial empty state', () => {
    expect(
      results(undefined, {})
    ).toEqual([]);
  });

  const res = results(undefined, {type: types.INIT_STATE, initState: testInitState});
  it('function should return an array', () => {
    expect(
      res
    ).toBeA('array');
  });
  if (res.length > 0) {
    it('function should return objects that have properties: gender, cats', () => {
      for (let index = 0; index < res.length; index++) {
        expect(
          res[index]
        ).toIncludeKeys(['gender', 'cats']);
      }
    });
    it('property cats should be an array', () => {
      for (let index = 0; index < res.length; index++) {
        expect(
          res[index].cats
        ).toBeA('array');
      }
    });
    it('cat names in cats array should be sorted', () => {
      for (let index = 0; index < res.length; index++) {
        expect(
          lodash.isEqual(res[index].cats, [...res[index].cats].sort())
        ).toBe(true);
      }
    });
  }
  it('test input should return exact test result', () => {
    expect(
      res
    ).toEqual(testInitStateResult);
  });
});
