import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../components/Header';
import Quiz from '../components/Quiz';
import * as SaveActions from '../actions';

class App extends Component {
  componentWillMount() {
    this.props.actions.getInitState();
  }
  render() {
    const { results, saved, actions } = this.props !== undefined ? this.props : {};

    return (
      <div>
        <Header />
        <div className="container">
          <Quiz results={results} saved={saved} actions={actions} />
        </div>
      </div>
    );
  }
}

App.propTypes = {
  actions: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    results: state.results,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(SaveActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
