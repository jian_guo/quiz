import React from 'react';
import style from './Header.css';

const Header = () =>
  <header className={style.header}>
    <div className={style.title}>
      <h1>Coding exercise - Jian Guo</h1>
    </div>
  </header>;

export default Header;
