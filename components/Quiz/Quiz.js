import React, { Component } from 'react';
import style from './Quiz.css';

class Quiz extends Component {
  render() {
    const { results } = this.props !== undefined ? this.props : {};

    return (
      <section className={style.quiz}>
        <h2>React Solution <small>React, Redux, Expect, Mocha, Webpack, etc.</small></h2>
        <div className="row">
          <div className="col-sm-12">
            {/* <List heading="Results" list={{ data: results, type: 'results' }} actions={actions} /> */}
            <p>
              Output a list of all the cats in alphabetical order under a heading of the gender of their owner
            </p>
            <figure className="highlight">
              <pre>
              {
                results.map((item, index) => {
                  return (
                    <div key={index}>
                      {item.gender}
                      <ul>
                      {
                        item.cats.map((cat, key) => <li key={key}>{cat}</li>)
                      }
                      </ul>
                    </div>
                  );
                })
              }
              </pre>
            </figure>
          </div>
        </div>
        <div id="json"></div>
      </section>
    );
  }
}

export default Quiz;
