
import * as types from '../constants/ActionTypes';
import fetchJson from '../utils/fetchJson';

export function getInitState() {
  return async (dispatch) => {
    const data = await fetchJson('http://jianguo.com.au/getJson.php');
    dispatch({ type: types.INIT_STATE, initState: data });
  };
}
