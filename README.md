# UI Quiz

## How to setup

Run the following commands in the root folder

```
npm install
npm start

open http://localhost:3000/ in your browser
```

I'm using Node v6.9.1 and npm v4.0.2 if you run into any trouble, try upgrade the node/npm version.

## How to run test

This code is using [expect](https://www.npmjs.com/package/expect) and [mocha](https://www.npmjs.com/package/mocha) for testing.

```
npm test
```

