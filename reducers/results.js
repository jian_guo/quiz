import { INIT_STATE } from '../constants/ActionTypes';
import lodash from 'lodash';

function getCats(rawJson) {
  const res = [];
  rawJson.map((item) => {
    item.pets.map(pet => pet.type === 'Cat' ? res.push(pet.name) : null);
  });
  return res.sort();
}

function parseRawJson(rawJson) {
  return lodash(rawJson)
    .filter({pets: [{type: 'Cat'}]})
    .groupBy(item => item.gender)
    .map((val, key) => ({gender: key, cats: getCats(val)}))
    .value();
}

export default function results(state = [], action) {
  switch (action.type) {
  case INIT_STATE:
    return parseRawJson([...action.initState]);
  default:
    return state;
  }
}


